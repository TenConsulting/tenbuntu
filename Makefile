cnf ?= config.env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

build:
	docker build -t ${COMPANY}/${APP}:${VERSION} ${DIR_SRC}

run:
	docker run -it -h workspace ${COMPANY}/${APP}:${VERSION}

deploy:
	docker push ${COMPANY}/${APP}:${VERSION}

clean:
	docker system prune -a
